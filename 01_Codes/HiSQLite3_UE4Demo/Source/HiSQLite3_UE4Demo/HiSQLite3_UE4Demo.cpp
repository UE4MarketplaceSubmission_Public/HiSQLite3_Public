/*
**	Copyright (c) 2016-2018 YeHaike(841660657@qq.com).
**	All rights reserved.
**	@ Date : 2017/11/21
*/

#include "HiSQLite3_UE4Demo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HiSQLite3_UE4Demo, "HiSQLite3_UE4Demo" );
