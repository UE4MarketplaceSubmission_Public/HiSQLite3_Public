/*
**	Copyright (c) 2016-2018 YeHaike(841660657@qq.com).
**	All rights reserved.
**	@ Date : 2017/11/21
*/

using UnrealBuildTool;
using System.Collections.Generic;

public class HiSQLite3_UE4DemoEditorTarget : TargetRules
{
	public HiSQLite3_UE4DemoEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "HiSQLite3_UE4Demo" } );
	}
}
